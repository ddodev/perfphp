
<?php
require '../src/TracePerfClass.php';
function p(){
    //usleep(rand(100000,1000000));
    usleep(rand(100,1000));
}
TracePerf::begin();
p();
$b = TracePerf::start('calcul1');
p();
$c = TracePerf::start('calcul2','arg');
p();
$c = TracePerf::step($c,'point1','comm');
p();
$b = TracePerf::step($b,'point1');
p();
$c = TracePerf::step($c,'point2');
p();
$c = TracePerf::step($c,'point3');
p();
TracePerf::stop($b);
p();
TracePerf::stop($c);
p();
TracePerf::end(); //optional
$ret = TracePerf::displayratio();

$ep=9;
$int1=3;
$int2=3;
$ratio = 800;
$left = 8 * $ep;
$inc=$ep+$int1+$int2;
$nbsteps=count($ret['steps']);
$height = $inc * $nbsteps;
?>
<svg height="<?=$height?>px" width="<?=($ratio+$left)?>px">
<?php
echo '<rect x="'.$left.'" height="'.$height.'px" width="'.$ratio.'px" fill="gainsboro" />';

for ($i = 0; $i < $nbsteps; $i++){
    $x = $left + $ret['steps'][$i]['start'] * $ratio;
    $y = ($i * $inc) + $int1;
    $cy = ($i * $inc) + ($inc/2) ;
    $width =  ($ret['steps'][$i]['stop'] - $ret['steps'][$i]['start']) * $ratio;
    $title = $ret['steps'][$i]['arg1'].' - '.$ret['steps'][$i]['arg2'];
    echo '<text x="0" y="'.($y + $ep).'" fill="black" font-size="'.$ep.'">'.$title.'</text>';   
    echo '<rect x="'.$x.'" y="'.$y.'" height="'.$ep.'px" width="'.$width.'px" fill="grey" title="'.$title.'"/>';
    
    $nbpoints = count($ret['steps'][$i]['steps']);
    for ($j = 0; $j < $nbpoints; $j++){
      $title = $ret['steps'][$i]['steps'][$j]['arg1'].' - '.$ret['steps'][$i]['steps'][$j]['arg2'];
      $cx  =  $left + $ret['steps'][$i]['steps'][$j]['time'] * $ratio;   
      echo '<circle cx="'.$cx.'" cy="'.$cy.'" r="'.(($inc/2)-1).'" fill="black" title="'.$title.'" />';
    }

}
?>        
</svg>


<pre>

<?php
print_r($ret);
