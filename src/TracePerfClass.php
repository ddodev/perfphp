<?php
/**
*   Class for timeline
*
*/
class TracePerf{

    private static $time_model = array ( 'start' => null,
                                         'stop' => null,
                                         'arg1' => '',
                                         'arg2' => '',
                                         'steps' => array()
                                        );
                                        
    private static $ret = array ( 'start' => null,
                                  'stop' => null,
                                  'steps' => array()  );
    
  
  
    /**
    *   BEGIN
    */
    public static function begin(){
        self::$ret['start'] = microtime(true);
    }   
    /**
    *   START
    */
    public static function start($arg1=null,$arg2=null){
        $ret = self::$time_model;
        $ret['start'] = microtime(true);
        $ret['arg1'] = $arg1;
        $ret['arg2'] = $arg2;
        return $ret;
    }
    /**
    *   STEP
    */
    public static function step($gr,$arg1='',$arg2=''){
        $gr['steps'][] = array('time' => microtime(true),
                               'arg1' => $arg1,
                               'arg2' => $arg2);
        return $gr;
    }
    /**
    *   STOP
    */
    public static function stop($gr){
        $gr['stop'] = microtime(true);
        self::$ret['steps'][] = $gr;
    }
    /**
    *   END
    */
    public static function end(){
        self::$ret['stop'] = microtime(true);
    }
    /**
    *   return trace
    */
    public static function display(){
        if ( self::$ret['stop'] == null ){
            self::$ret['stop'] = microtime(true);
        }
        return self::$ret;
    }
    public static function displayratio(){
        $ret = self::display();
        $x1 = $ret['start'];
        $x2 = $ret['stop'];
        $ratio = 1 / ($x2 - $x1);
        
        $length = count($array);
        for ($i = 0; $i < count($ret['steps']); $i++) {
            if($ret['steps'][$i]['start'] != null){
                $ret['steps'][$i]['start'] = ($ret['steps'][$i]['start'] - $x1) * $ratio;
            }
           if($ret['steps'][$i]['stop'] != null){
                $ret['steps'][$i]['stop'] = ($ret['steps'][$i]['stop'] - $x1) * $ratio;
            }
            for ($j = 0; $j < count($ret['steps'][$i]['steps']); $j++) {
                if($ret['steps'][$i]['steps'][$j]['time'] != null){
                    $ret['steps'][$i]['steps'][$j]['time'] = ($ret['steps'][$i]['steps'][$j]['time'] - $x1) * $ratio;
                }
            }                
        }
        $ret['start'] = ($ret['start'] - $x1) * $ratio;
        $ret['stop'] = ($ret['stop'] - $x1) * $ratio;
        return $ret;
    }

}